﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveForward : ICommand
{
    Transform transform;
    float offsetValue;

    public MoveForward(Transform transform, float offsetValue)
    {
        this.transform = transform;
        this.offsetValue = offsetValue;
    }

    public void Invoke()
    {
        transform.position = new Vector3(0, transform.position.y + offsetValue, 0);
    }

    public void Undo()
    {
        transform.position = new Vector3(0, transform.position.y - offsetValue, 0);
    }

}
