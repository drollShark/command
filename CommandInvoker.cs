﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandInvoker : MonoBehaviour
{
    private List<ICommand> commands;

    public CommandInvoker()
    {
        commands = new List<ICommand>();
    }

    public void AddCommand(ICommand command)
    {
        if(command != null)
        {
            commands.Add(command);
        }
    }

    public void RemoveCommand(ICommand command)
    {
        if(command != null)
        {
            commands.Remove(command);
        }
    }

    public void ProcessAll()
    {
        for (int i = 0; i < commands.Count; i++)
        {
            commands[i].Invoke();
        }
    }

    public void Process()
    {
        if(commands.Count >= 0)
        {
            int index = commands.Count;
            commands[index - 1].Invoke();
        }
    }

    public void Undo()
    {
        if(commands.Count >= 0)
        {
            int index = commands.Count;
            commands[index - 1].Undo();
            commands.RemoveAt(index - 1 < 0 ? 0 : index - 1);
        }
    }

    public void ClearQueue()
    {
        commands.Clear();
    }

}
