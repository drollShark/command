﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : ICommand
{
    Transform transform;
    float offsetValue;

    public Rotate(Transform transform, float offsetValue)
    {
        this.transform = transform;
        this.offsetValue = offsetValue;
    }

    public void Invoke()
    {
        transform.Rotate(new Vector3(0, transform.position.y + offsetValue, 0));
    }

    public void Undo()
    {
        transform.Rotate(new Vector3(0, transform.position.y - offsetValue, 0));
    }
}
