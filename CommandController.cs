﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CommandController : MonoBehaviour
{
    private CommandInvoker commandInvoker;

    private void OnEnable()
    {
        commandInvoker = new CommandInvoker();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            MoveForward moveForward = new MoveForward(this.transform, 1.0f);
            commandInvoker.AddCommand(moveForward);
        }

        if(Input.GetKeyDown(KeyCode.A))
        {
            Rotate leftRotate = new Rotate(this.transform, -45);
            commandInvoker.AddCommand(leftRotate);
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            Rotate rightRotate = new Rotate(this.transform, 45);
            commandInvoker.AddCommand(rightRotate);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            commandInvoker.ProcessAll();
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            commandInvoker.Process();
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            commandInvoker.Undo();
        }
    }
}
